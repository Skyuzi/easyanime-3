﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Dto.Order
{
    public class OrderDto
    {
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int Status { get; set; }
        public string BookName { get; set; }
        public string BookNameForRoute { get; set; }
        public DateTime AddedTime { get; set; }
    }
}
