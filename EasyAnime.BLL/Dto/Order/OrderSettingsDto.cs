﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EasyAnime.BLL.Dto.Order
{
    public class OrderSettingsDto
    {
        public int OrderId { get; set; }

        [Range(1, 3, ErrorMessage = "Пожалуйста, проверьте валидность статуса")]
        public int Status { get; set; }
    }
}
