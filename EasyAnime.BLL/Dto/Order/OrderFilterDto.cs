﻿using EasyAnime.BLL.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Dto.Order
{
    public class OrderFilterDto : CommonDto
    {
        public int UserId { get; set; }
        public string BookName { get; set; }
    }
}
