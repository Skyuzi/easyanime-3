﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EasyAnime.BLL.Dto.Order
{
    public class MakeOrder
    {
        [Required(ErrorMessage = "Пожалуйста, введите Email")]
        [EmailAddress(ErrorMessage = "Пожалуйста, проверьте правильность Email")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "Email не может быть меньше 6 и больше 30 символов")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите адрес")]
        [StringLength(150, MinimumLength = 6, ErrorMessage = "Адрес не может быть меньше 6 и больше 150 символов")]
        public string Address { get; set; }

        public string BookName { get; set; }
    }
}
