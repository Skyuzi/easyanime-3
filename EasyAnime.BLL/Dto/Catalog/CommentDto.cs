﻿using EasyAnime.BLL.Dto.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EasyAnime.BLL.Dto.Catalog
{
    public class CommentDto
    {
        public int Id { get;set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserAvatar { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите комментарий")]
        [StringLength(50, MinimumLength = 10, ErrorMessage = "Комментарий не может быть меньше 10 или больше 50 символов")]
        public string CommentText { get; set; }
        public string BookNameForRoute { get; set; }
        public string AddedTime { get; set; }
    }
}
