﻿using EasyAnime.BLL.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Dto.Catalog
{
    public class CategoryDto
    {
        public string CategoryName { get; set; }
        public int? Page { get; set; }
        public string BookName { get; set; }
        public SortState SortOrder { get; set; }

    }
}
