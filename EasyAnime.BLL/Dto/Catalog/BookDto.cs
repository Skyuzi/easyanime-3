﻿using EasyAnime.BLL.Dto.Catalog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EasyAnime.BLL.Dto.Catalog
{
    public class BookDto
    {
        [StringLength(maximumLength: 50, ErrorMessage = "Название не может быть меньше трех символов", MinimumLength = 3)]
        public string Name { get; set; }
        public string NameForRoute { get; set; }
        public string Author { get; set; }
        [Required(ErrorMessage = "Введите год")]
        [Range(1950, 2020, ErrorMessage = "Год не может быть меньше 1950 и больше 2020")]
        public int? Year { get; set; }
        public string ImageUrl { get; set; }
        [Required(ErrorMessage = "Введите цену")]
        [Range(100, 100000, ErrorMessage = "Цена не может быть меньше 100 и больше 100000 рублей")]
        public int? Price { get; set; }
        public string GenresForAdd { get; set; }
        public List<GenreNamesDto> Genres { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
    }
}
