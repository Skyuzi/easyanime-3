﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Dto.Catalog
{
    public class GenreNamesDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameForRoute { get; set; }
    }
}
