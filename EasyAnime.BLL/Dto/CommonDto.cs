﻿using EasyAnime.BLL.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Dto
{
    public class CommonDto
    {
        public int? Page { get; set; }
        public SortState SortOrder { get; set; }
    }
}
