﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Dto.Account
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public bool IsBan { get; set; }
        public bool IsDeleted { get; set; }
        public string Email { get; set; }
        public string IpAddress { get; set; }
        public string BanDescription { get; set; }
        public DateTime AccountCreated { get; set; }
        public DateTime LastEnter { get; set; }
    }
}
