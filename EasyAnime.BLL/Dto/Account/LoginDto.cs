﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EasyAnime.BLL.Dto.Account
{
    public class LoginDto : InfoDto
    {
        [Required (ErrorMessage = "Введите логин")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "Длина логина должна быть от 5 до 20 символов")]
        public string Login { get; set; }

        [Required (ErrorMessage = "Введите пароль")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Длина пароля должна быть от 5 до 50 символов")]
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
    }
}
