﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Dto.Account
{
    public class ChangeAvatarDto
    {
        public IFormFile File { get; set; }
        public int UserId { get; set; }
        public string WebPath { get; set; }
    }
}
