﻿using EasyAnime.BLL.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Dto.Admin
{
    public class UsersListDto : CommonDto
    {
        public string Login { get; set; } = string.Empty;
        public UserListFilters Filter { get; set; }
    }
}
