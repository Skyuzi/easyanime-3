﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EasyAnime.BLL.Dto.Admin
{
    public class UserSettingsDto
    {
        public string Login { get; set; }
        public int RoleId { get; set; }
        public string IpAddress { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsBan { get; set; }
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Описание не может быть меньше 6 и больше 50 символов")]
        public string BanDescription { get; set; }
    }
}
