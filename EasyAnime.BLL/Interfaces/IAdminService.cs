﻿using EasyAnime.BLL.Dto.Account;
using EasyAnime.BLL.Dto.Admin;
using EasyAnime.BLL.Dto.Catalog;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Interfaces
{
    public interface IAdminService
    {
        IEnumerable<UserDto> GetUsersList(UsersListDto dto);
        UserDto GetUserById(int id);
        UserDto ChangeSettings(UserSettingsDto dto);
        void DeleteComment(int commentId);
        void AddBookOrSave(BookDto dto);
        IEnumerable<GenreNamesDto> GetGenreNames();

        BookDto GetBook(string bookName);
    }
}
