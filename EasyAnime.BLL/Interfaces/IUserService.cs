﻿using EasyAnime.BLL.Dto.Account;
using EasyAnime.DAL.Entities.Users;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace EasyAnime.BLL.Interfaces
{
    public interface IUserService
    {
        UserDto Login(LoginDto dto);
        UserDto Register(RegisterDto dto);
        ClaimsIdentity GetClaimsForAuthentication(UserDto user);
        void ChangeAvatar(ChangeAvatarDto dto);
        string GetAvatarName(int userId);
    }
}
