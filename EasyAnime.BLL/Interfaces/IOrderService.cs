﻿using EasyAnime.BLL.Dto.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Interfaces
{
    public interface IOrderService
    {
        OrderDto GetOrder(OrderFilterDto filter);
        void Make(OrderDto dto);
        IEnumerable<OrderDto> GetOrdersById(OrderFilterDto filter);
        IEnumerable<OrderDto> GetOrders(OrderFilterDto filter);
        OrderDto GetOrderById(int id);
        void ChangeOrder(OrderSettingsDto dto);
    }
}
