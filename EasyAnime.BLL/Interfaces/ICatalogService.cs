﻿using EasyAnime.BLL.Dto.Admin;
using EasyAnime.BLL.Dto.Catalog;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Interfaces
{
    public interface ICatalogService
    {
        IEnumerable<GenreNamesDto> GetGenreNames();
        GenreNamesDto GetGenreNameByNameForRoute(string nameForRoute);
        IEnumerable<BookDto> GetBooksForCategory(CategoryDto dto);
        void AddComment(CommentDto dto);
        BookDto GetBookForItem(string itemName);
        IEnumerable<CommentDto> GetCommentsForBook(string bookNameForRoute);
    }
}
