﻿using EasyAnime.BLL.Dto.Account;
using EasyAnime.BLL.Dto.Admin;
using EasyAnime.BLL.Dto.Catalog;
using EasyAnime.BLL.Helpers.Enums;
using EasyAnime.BLL.Helpers.Extensions;
using EasyAnime.BLL.Interfaces;
using EasyAnime.DAL.Entities.Books;
using EasyAnime.DAL.Entities.Users;
using EasyAnime.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.BLL.Services
{
    public class AdminService : IAdminService
    {
        private IUnitOfWork _unitOfWork;

        public AdminService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddBookOrSave(BookDto dto)
        {
            var book = _unitOfWork.Books.GetByNameForRoute(dto.NameForRoute);
            if (book != null)
            {
                book.Name = dto.Name;
                book.NameForRoute = dto.NameForRoute;
                book.Author = dto.Author;
                book.Year = dto.Year.Value;
                book.Price = dto.Price.Value;
                book.Description = dto.Description;
                book.ImageUrl = dto.ImageUrl;
                book.AddedTime = DateTime.Now;
                book.ChangedTime = DateTime.Now;
            }
            else
            {
                var genresFromDto = dto.GenresForAdd.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => int.Parse(x));

                var newBook = new Book
                {
                    Name = dto.Name,
                    NameForRoute = dto.NameForRoute,
                    Author = dto.Author,
                    Year = dto.Year.Value,
                    Price = dto.Price.Value,
                    Description = dto.Description,
                    ImageUrl = dto.ImageUrl,
                    AddedTime = DateTime.Now,
                    ChangedTime = DateTime.Now,
                    DiscountPercent = 0,
                    IsDiscount = false
                };

                _unitOfWork.Books.Add(newBook);
                _unitOfWork.Save();

                var genres = new List<Genres>();

                foreach (var item in genresFromDto)
                {
                    var genre = new Genres { BookId = newBook.Id, GenreNamesId = item };
                    genres.Add(genre);
                }

                _unitOfWork.Genres.AddRange(genres);
            }

            _unitOfWork.Save();
        }

        public UserDto ChangeSettings(UserSettingsDto dto)
        {
            var user = _unitOfWork.Users.GetByLogin(dto.Login);
            if (user == null)
                throw new Exception("Пользователь не найден!");

            if (user.IsBan && !dto.IsBan)
            {
                var banUser = _unitOfWork.BannedUsers.GetLastByUserId(user.Id);
                banUser.IsActual = false;

                _unitOfWork.Save();
            }
            else if (!user.IsBan && dto.IsBan)
            {
                var bannedUser = new BannedUser
                {
                    UserId = user.Id,
                    IsActual = true,
                    Description = dto.BanDescription
                };

                _unitOfWork.BannedUsers.Add(bannedUser);
                _unitOfWork.Save();
            }

            user.RoleId = dto.RoleId;
            user.IsBan = dto.IsBan;
            user.IsDeleted = dto.IsDeleted;

            _unitOfWork.Save();

            return user.ToUserDto();
        }

        public void DeleteComment(int commentId)
        {
            var comment = _unitOfWork.Comments.GetById(commentId);
            if (comment == null)
                throw new Exception("Произошла ошибка");

            _unitOfWork.Comments.Remove(comment);
            _unitOfWork.Save();
        }

        public BookDto GetBook(string bookName)
        {
            return _unitOfWork.Books.GetByNameForRoute(bookName).ToBookDto();
        }

        public IEnumerable<GenreNamesDto> GetGenreNames()
        {
            return _unitOfWork.GenreNames.GetAll().ToGenreNamesDto();
        }

        public UserDto GetUserById(int id)
        {
            var user = _unitOfWork.Users.GetById(id);
            if (user == null)
                return null;

            var banDescription = string.Empty;
            if (user.IsBan)
            {
                var bannedUser = _unitOfWork.BannedUsers.GetByUserId(id);
                if (bannedUser != null)
                    banDescription = bannedUser.Description;
            }

            var userDto = user.ToUserDto();
            userDto.BanDescription = banDescription;

            return userDto;
        }

        public IEnumerable<UserDto> GetUsersList(UsersListDto dto)
        {
            IEnumerable<User> users;
            switch (dto.Filter)
            {
                case UserListFilters.All:
                    users = _unitOfWork.Users.GetAll();
                    break;
                case UserListFilters.Active:
                    users = _unitOfWork.Users.GetActual();
                    break;
                case UserListFilters.NotActive:
                    users = _unitOfWork.Users.GetNotActual();
                    break;
                case UserListFilters.Banned:
                    users = _unitOfWork.Users.GetBanned();
                    break;
                case UserListFilters.NotBanned:
                    users = _unitOfWork.Users.GetNotBanned();
                    break;
                default:
                    users = _unitOfWork.Users.GetAll();
                    break;
            }

            return users.Where(x => x.Login.Contains(dto.Login)).OrderByFilter(dto.SortOrder).ToUserDto();
        }
    }
}
