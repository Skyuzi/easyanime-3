﻿using EasyAnime.BLL.Dto.Account;
using EasyAnime.BLL.Helpers.Constants;
using EasyAnime.BLL.Helpers.Extensions;
using EasyAnime.BLL.Interfaces;
using EasyAnime.DAL.Entities.Users;
using EasyAnime.DAL.Interfaces;
using Microsoft.AspNetCore.Authentication.Cookies;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Text;

namespace EasyAnime.BLL.Services
{
    public class UserService : IUserService
    {
        private IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public UserDto Login(LoginDto dto)
        {
            var user = _unitOfWork.Users.GetByLoginAndPassword(dto.Login, dto.Password);
            if (user == null)
                throw new Exception("Пожалуйста, проверьте правильность введенных данных");

            return user.ToUserDto();
        }

        public UserDto Register(RegisterDto dto)
        {
            if (_unitOfWork.Users.IsExist(dto.Login))
                throw new Exception("Такой аккаунт уже существует!");

            var user = new User
            {
                Login = dto.Login,
                Password = dto.Password,
                RoleId = RoleTypes.User,
                IsBan = false,
                IsDeleted = false,
                AccountCreated = DateTime.Now,
                LastEnter = DateTime.Now,
            };

            _unitOfWork.Users.Add(user);
            _unitOfWork.Save();

            return user.ToUserDto();
        }

        public ClaimsIdentity GetClaimsForAuthentication(UserDto user)
        {
            var roleName = _unitOfWork.Roles.GetById(user.RoleId).Name;

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, roleName),
                new Claim("Id", user.Id.ToString())
            };

            return new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public void ChangeAvatar(ChangeAvatarDto dto)
        {
            if (dto.File == null)
                throw new Exception("Выберите аватар");


            var pathToDirectoryAvatars = $"{dto.WebPath}/data/avatars/";
            var oldAvatar = _unitOfWork.Avatars.GetByUserId(dto.UserId);
            if (oldAvatar != null)
            {
                var pathToOldAvatar = pathToDirectoryAvatars + oldAvatar.AvatarName;
                var oldAvatarFile = new FileInfo(pathToOldAvatar);
                if (oldAvatarFile.Exists)
                    oldAvatarFile.Delete();

                _unitOfWork.Avatars.Remove(oldAvatar);
                _unitOfWork.Save();
            }

            var fileName = $"{dto.UserId}.jpg";

            using (var fileStream = new FileStream(pathToDirectoryAvatars + fileName, FileMode.Create))
                dto.File.CopyTo(fileStream);

            var avatar = new Avatar
            {
                AvatarName = fileName,
                UserId = dto.UserId
            };

            _unitOfWork.Avatars.Add(avatar);
            _unitOfWork.Save();
        }

        public string GetAvatarName(int userId)
        {
            var avatar = _unitOfWork.Avatars.GetByUserId(userId);
            if (avatar == null)
                throw new Exception("Ошибка!");

            return avatar.AvatarName;
        }
    }
}
