﻿using EasyAnime.BLL.Dto.Order;
using EasyAnime.BLL.Helpers.Constants;
using EasyAnime.BLL.Helpers.Extensions;
using EasyAnime.BLL.Interfaces;
using EasyAnime.DAL.Entities.Orders;
using EasyAnime.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.BLL.Services
{
    public class OrderService : IOrderService
    {
        private IUnitOfWork _unitOfWork;

        public OrderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public OrderDto GetOrder(OrderFilterDto filter)
        {
            var user = _unitOfWork.Users.GetById(filter.UserId);
            var lastOrder = _unitOfWork.Orders.GetByUserId(filter.UserId).LastOrDefault();
            var orderDto = new OrderDto
            {
                Email = user.Email,
                BookName = filter.BookName
            };

            if (lastOrder != null)
                orderDto.Address = lastOrder.Address;

            return orderDto;
        }

        public void Make(OrderDto dto)
        {
            var book = _unitOfWork.Books.GetByNameForRoute(dto.BookName);
            if (book == null)
                throw new Exception("Ошибка!");

            var user = _unitOfWork.Users.GetById(dto.UserId);
            if (!string.IsNullOrEmpty(dto.Email) && user.Email != dto.Email)
            {
                user.Email = dto.Email;
                _unitOfWork.Save();
            }

            var order = new Order
            {
                UserId = dto.UserId,
                BookId = book.Id,
                Address = dto.Address,
                Status = OrderStatus.Process,
                AddedTime = DateTime.Now
            };

            _unitOfWork.Orders.Add(order);
            _unitOfWork.Save();
        }

        public IEnumerable<OrderDto> GetOrdersById(OrderFilterDto filter)
        {
            var orders = _unitOfWork.Orders.GetByUserId(filter.UserId);

            return orders.OrderByFilter(filter.SortOrder).ToOrderDto();
        }

        public OrderDto GetOrderById(int id)
        {
            return _unitOfWork.Orders.GetById(id).ToOrderDto();
        }

        public void ChangeOrder(OrderSettingsDto dto)
        {
            var order = _unitOfWork.Orders.GetById(dto.OrderId);
            if (order == null)
                throw new Exception("Заказ не найден");

            order.Status = dto.OrderId;
            _unitOfWork.Save();
        }

        public IEnumerable<OrderDto> GetOrders(OrderFilterDto filter)
        {
            var orders = _unitOfWork.Orders.GetAll();

            return orders.OrderByFilter(filter.SortOrder).ToOrderDto();
        }
    }
}
