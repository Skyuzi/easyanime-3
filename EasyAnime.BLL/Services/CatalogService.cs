﻿using EasyAnime.BLL.Dto.Admin;
using EasyAnime.BLL.Dto.Catalog;
using EasyAnime.BLL.Helpers.Extensions;
using EasyAnime.BLL.Interfaces;
using EasyAnime.DAL.Entities.Books;
using EasyAnime.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.BLL.Services
{
    public class CatalogService : ICatalogService
    {
        private IUnitOfWork _unitOfWork;

        public CatalogService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddComment(CommentDto dto)
        {
            var book = _unitOfWork.Books.GetByNameForRoute(dto.BookNameForRoute);
            if (book == null)
                throw new Exception("Произошла ошибка!");

            var comment = new Comment
            {
                Text = dto.CommentText,
                BookId = book.Id,
                UserId = dto.UserId,
                AddedTime = DateTime.Now
            };

            _unitOfWork.Comments.Add(comment);
            _unitOfWork.Save();
        }

        public IEnumerable<BookDto> GetBooksForCategory(CategoryDto dto)
        {
            var genre = _unitOfWork.GenreNames.GetByNameForRoute(dto.CategoryName);
            if (genre == null)
                return null;

            var books = !string.IsNullOrEmpty(dto.BookName) 
                ? _unitOfWork.Genres.GetBooksByGenreNamesIdAndByBookName(genre.Id, dto.BookName) 
                : _unitOfWork.Genres.GetBooksByGenreNamesId(genre.Id);

            return books.OrderByFilter(dto.SortOrder).ToBookDto();
        }

        public BookDto GetBookForItem(string itemName)
        {
            var book = _unitOfWork.Books.GetByNameForRoute(itemName);
            if (book == null)
                return null;

            var genres = new List<GenreNamesDto>();
            foreach (var genreFromList in book.Genres)
            {
                var genre = _unitOfWork.GenreNames.GetById(genreFromList.GenreNamesId).ToGenreNamesDto();
                genres.Add(genre);
            }

            var bookDto = book.ToBookDto();
            bookDto.Genres = genres;

            return bookDto;
        }

        public GenreNamesDto GetGenreNameByNameForRoute(string nameForRoute)
        {
            return _unitOfWork.GenreNames.GetByNameForRoute(nameForRoute).ToGenreNamesDto();
        }

        public IEnumerable<GenreNamesDto> GetGenreNames()
        {
            return _unitOfWork.GenreNames.GetAll().ToGenreNamesDto();
        }

        public IEnumerable<CommentDto> GetCommentsForBook(string bookNameForRoute)
        {
            var book = _unitOfWork.Books.GetByNameForRoute(bookNameForRoute);
            return _unitOfWork.Comments.GetByBookId(book.Id).ToCommentDto();
        }
    }
}
