﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Helpers.Enums
{
    public enum SortState
    {
        NameAsc,
        AgeAsc,
        AgeDesc,
        StatusAsc,
        StatusDesc
    }
}
