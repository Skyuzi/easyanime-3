﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Helpers.Enums
{
    public enum UserListFilters
    {
        All,
        Active,
        NotActive,
        Banned,
        NotBanned
    }
}
