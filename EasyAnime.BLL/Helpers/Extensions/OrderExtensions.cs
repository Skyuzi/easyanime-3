﻿using EasyAnime.BLL.Dto.Order;
using EasyAnime.BLL.Helpers.Enums;
using EasyAnime.DAL.Entities.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.BLL.Helpers.Extensions
{
    public static class OrderExtensions
    {
        public static IEnumerable<Order> OrderByFilter(this IEnumerable<Order> orders, SortState sort)
        {
            switch (sort)
            {
                case SortState.AgeAsc:
                    return orders.OrderBy(x => x.AddedTime);
                case SortState.AgeDesc:
                    return orders.OrderByDescending(x => x.AddedTime);
                case SortState.StatusAsc:
                    return orders.OrderBy(x => x.Status);
                case SortState.StatusDesc:
                    return orders.OrderByDescending(x => x.Status);
                default:
                    return orders.OrderByDescending(x => x.AddedTime);
            }
        }

        public static IEnumerable<OrderDto> ToOrderDto(this IEnumerable<Order> orders)
        {
            var orderDtos = new List<OrderDto>();
            foreach (var order in orders)
            {
                orderDtos.Add(new OrderDto
                {
                    OrderId = order.Id,
                    Login = order.User.Login,
                    Address = order.Address,
                    Email = order.User.Email,
                    Status = order.Status,
                    BookName = order.Book.Name,
                    BookNameForRoute = order.Book.NameForRoute,
                    AddedTime = order.AddedTime
                });
            }

            return orderDtos;
        }

        public static OrderDto ToOrderDto(this Order order)
        {
            return new OrderDto
            {
                OrderId = order.Id,
                Login = order.User.Login,
                Address = order.Address,
                Email = order.User.Email,
                Status = order.Status,
                BookName = order.Book.Name,
                BookNameForRoute = order.Book.NameForRoute,
                AddedTime = order.AddedTime
            };
        }
    }
}
