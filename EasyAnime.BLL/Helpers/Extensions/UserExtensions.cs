﻿using EasyAnime.BLL.Dto.Account;
using EasyAnime.BLL.Dto.Admin;
using EasyAnime.BLL.Helpers.Enums;
using EasyAnime.DAL.Entities.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.BLL.Helpers.Extensions
{
    public static class UserExtensions
    {
        public static UserDto ToUserDto(this User user)
        {
            return new UserDto
            {
                Id = user.Id,
                Login = user.Login,
                Password = user.Password,
                RoleId = user.RoleId,
                IsBan = user.IsBan,
                IsDeleted = user.IsDeleted,
                Email = user.Email,
                IpAddress = user.IpAddress,
                AccountCreated = user.AccountCreated,
                LastEnter = user.LastEnter
            };
        }

        public static IEnumerable<UserDto> ToUserDto(this IEnumerable<User> users)
        {
            var userDtos = new List<UserDto>();
            foreach (var user in users)
            {
                var userDto = new UserDto
                {
                    Id = user.Id,
                    Login = user.Login,
                    Password = user.Password,
                    RoleId = user.RoleId,
                    IsBan = user.IsBan,
                    IsDeleted = user.IsDeleted,
                    Email = user.Email,
                    IpAddress = user.IpAddress,
                    AccountCreated = user.AccountCreated,
                    LastEnter = user.LastEnter
                };
                userDtos.Add(userDto);
            }

            return userDtos;
        }

        public static IEnumerable<User> OrderByFilter(this IEnumerable<User> users, SortState sort)
        {
            switch (sort)
            {
                case SortState.NameAsc:
                    return users.OrderBy(x => x.Login);
                case SortState.AgeAsc:
                    return users.OrderBy(x => x.AccountCreated);
                case SortState.AgeDesc:
                    return users.OrderByDescending(x => x.AccountCreated);
                default:
                    return users.OrderBy(x => x.Login);
            }
        }
    }
}
