﻿using EasyAnime.BLL.Dto.Admin;
using EasyAnime.BLL.Dto.Catalog;
using EasyAnime.BLL.Helpers.Enums;
using EasyAnime.DAL.Entities.Books;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.BLL.Helpers.Extensions
{
    public static class CatalogExtensions
    {
        public static GenreNamesDto ToGenreNamesDto(this GenreNames genreName)
        {
            return new GenreNamesDto
            {
                Id = genreName.Id,
                Name = genreName.Name,
                NameForRoute = genreName.NameForRoute
            };
        }

        public static IEnumerable<GenreNamesDto> ToGenreNamesDto(this IEnumerable<GenreNames> genreNames)
        {
            var genreNamesDtos = new List<GenreNamesDto>();
            foreach (var genreName in genreNames)
            {
                var userDto = new GenreNamesDto
                {
                    Id = genreName.Id,
                    Name = genreName.Name,
                    NameForRoute = genreName.NameForRoute
                };
                genreNamesDtos.Add(userDto);
            }

            return genreNamesDtos;
        }

        public static IEnumerable<BookDto> ToBookDto(this IEnumerable<Book> books)
        {
            var bookDtos = new List<BookDto>();
            foreach (var book in books)
            {
                var bookDto = new BookDto
                {
                    Name = book.Name,
                    NameForRoute = book.NameForRoute,
                    Author = book.Author,
                    Year = book.Year,
                    Price = book.Price,
                    ImageUrl = book.ImageUrl,
                    Description = book.Description
                };
                bookDtos.Add(bookDto);
            }

            return bookDtos;
        }

        public static BookDto ToBookDto(this Book book)
        {
            return new BookDto
            {
                Name = book.Name,
                NameForRoute = book.NameForRoute,
                Author = book.Author,
                Year = book.Year,
                Price = book.Price,
                ImageUrl = book.ImageUrl,
                Description = book.Description
            };
        }

        public static IEnumerable<Book> OrderByFilter(this IEnumerable<Book> books, SortState sort)
        {
            switch (sort)
            {
                case SortState.NameAsc:
                    return books.OrderBy(x => x.Name);
                case SortState.AgeAsc:
                    return books.OrderBy(x => x.Year);
                case SortState.AgeDesc:
                    return books.OrderByDescending(x => x.Year);
                default:
                    return books.OrderBy(x => x.Name);
            }
        }

        public static IEnumerable<CommentDto> ToCommentDto(this IEnumerable<Comment> comments)
        {
            var commentDtos = new List<CommentDto>();
            if (comments != null)
            {
                foreach (var comment in comments)
                {
                    var commentDto = new CommentDto
                    {
                        Id = comment.Id,
                        AddedTime = comment.AddedTime.ToLongDateString(),
                        UserId = comment.Id,
                        CommentText = comment.Text,
                        UserName = comment.User.Login
                    };

                    if (comment.User.Avatar != null)
                        commentDto.UserAvatar = comment.User.Avatar.AvatarName;

                    commentDtos.Add(commentDto);
                }
            }
            return commentDtos;
        }

        public static CommentDto ToCommentDto(this Comment comment)
        {
            return new CommentDto
            {
                Id = comment.Id,
                AddedTime = comment.AddedTime.ToLongDateString(),
                UserId = comment.Id,
                CommentText = comment.Text,
                UserAvatar = comment.User.Avatar.AvatarName,
                UserName = comment.User.Login
            };
        }
    }
}
