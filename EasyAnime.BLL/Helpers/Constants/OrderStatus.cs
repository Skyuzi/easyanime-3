﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Helpers.Constants
{
    public class OrderStatus
    {
        public static int Process = 0;
        public static int Sent = 1;
        public static int Ready = 2;
    }
}
