﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.BLL.Helpers.Constants
{
    public class RoleTypes
    {
        public static int User = 1;
        public static int Moderator = 2;
        public static int Admin = 3;
    }
}
