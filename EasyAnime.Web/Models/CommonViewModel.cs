﻿using EasyAnime.BLL.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyAnime.Web.Models
{
    public class CommonViewModel
    {
        public int CountOnPage { get; set; }
        public int? Page { get; set; }
        public int TotalCount { get; set; }
        public string Search { get; set; }
        public SortState SortOrder { get; set; }
        public Tuple<string, SortState>[] Sort { get; set; }
    }
}
