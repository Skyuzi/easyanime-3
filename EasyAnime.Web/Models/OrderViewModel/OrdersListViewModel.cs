﻿using EasyAnime.BLL.Dto.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyAnime.Web.Models.OrderViewModel
{
    public class OrdersListViewModel : CommonViewModel
    {
        public IEnumerable<OrderDto> OrderDtos { get; set; }
    }
}
