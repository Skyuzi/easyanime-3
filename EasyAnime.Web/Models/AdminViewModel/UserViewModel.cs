﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyAnime.Web.Models.AdminViewModel
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsBan { get; set; }
        public bool IsDeleted { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public string IpAddress { get; set; }
        public string BanDescription { get; set; }
        public DateTime AccountCreated { get; set; }
        public DateTime LastEnter { get; set; }
    }
}
