﻿using EasyAnime.BLL.Dto.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace EasyAnime.Web.Models.AdminViewModel
{
    public class UsersListViewModel : CommonViewModel
    {
        public IPagedList<UserDto> UserViewModels { get; set; }
    }
}
