﻿using EasyAnime.BLL.Dto.Admin;
using EasyAnime.BLL.Dto.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace EasyAnime.Web.Models.CatalogViewModel
{
    public class CategoryViewModel : CommonViewModel
    {
        public IEnumerable<BookDto> BookViewModels {get;set;}
        public string GenreName { get; set; }
        public string GenreNameForRoute { get; set; }
    }
}
