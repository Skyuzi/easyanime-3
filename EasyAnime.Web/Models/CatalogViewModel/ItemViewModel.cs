﻿using EasyAnime.BLL.Dto.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyAnime.Web.Models.CatalogViewModel
{
    public class ItemViewModel
    {
        public BookDto Book { get; set; }
        public IEnumerable<CommentDto> Comments { get; set; }
    }
}
