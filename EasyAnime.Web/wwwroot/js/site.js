﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function NewParameterInUrlFromSelect(selectClass, parName) {
    var selected = $("." + selectClass + " option:selected").val();
    var link = document.location.href;
    if (link.includes("&" + parName + "=")) {
        link = link.split("&" + parName + "=")[0];
        link += "&" + parName + "=";
    }
    else if (link.includes("?" + parName + "=")) {
        link = link.split("?" + parName + "=")[0];
        link += "?" + parName + "=";
    }
    else if (!link.includes("?")) {
        link += "?" + parName + "=";
    }
    else {
        link += "&" + parName + "=";
    }
    document.location.href = link + selected;
}

function FindByEmail() {
    var email = $(".searchEmail").val();
    document.location.href = "/Order/MyOrders?Email=" + email;
}
