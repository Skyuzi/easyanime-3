﻿function FindByName(categoryName) {
    var bookName = $(".searchBook").val();
    document.location.href = "/Catalog/Category/" + categoryName + "?bookName=" + bookName;
}

function MakeOrder() {
    var email = $("input[name='Email']").val();
    var address = $("input[name='Address']").val();
    var bookName = $("input[name='BookName']").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        data: { Email: email, Address: address, BookName: bookName },
        url: "/Order/Make",
        success: function (result) {
            if (result.success) {
                $('.modal-body').addClass("text-success").html(result.data);
            }
            else {
                $('.info').html("");
                result.data.forEach(function (item) {
                    $('.info').addClass("text-danger").append(item + "<br>");
                });
            }
        }
    });
}

function AddComment() {
    var commentText = $("textarea[name='CommentText']").val();
    var bookNameForRoute = $("input[name='BookNameForRoute']").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        data: { CommentText: commentText, BookNameForRoute: bookNameForRoute },
        url: "/Catalog/AddComment",
        success: function (result) {
            if (result.success) {
                $('.modal-body').addClass("text-success").html(result.data);
            }
            else {
                $('.info').html("");
                result.data.forEach(function (item) {
                    $('.info').addClass("text-danger").append(item + "<br>");
                });
            }
        }
    });
}

function DeleteComment(commentId) {

    $.ajax({
        type: "POST",
        dataType: "json",
        data: { CommentId: commentId },
        url: "/Admin/DeleteComment",
        success: function (result) {
            if (result.success) {
                document.location.reload();
            }
            else {
                $('.modal-body').addClass("text-danger").html(result.data);
            }
        }
    });
}