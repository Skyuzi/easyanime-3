#pragma checksum "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f611440331d97a794942357c9e2b3c31efe63b85"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Order_OrdersList), @"mvc.1.0.view", @"/Views/Order/OrdersList.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Order/OrdersList.cshtml", typeof(AspNetCore.Views_Order_OrdersList))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\C#\EasyAnime\EasyAnime.Web\Views\_ViewImports.cshtml"
using EasyAnime.Web;

#line default
#line hidden
#line 2 "D:\C#\EasyAnime\EasyAnime.Web\Views\_ViewImports.cshtml"
using EasyAnime.Web.Models;

#line default
#line hidden
#line 2 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
using EasyAnime.BLL.Helpers.Constants;

#line default
#line hidden
#line 3 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
using X.PagedList.Mvc.Core;

#line default
#line hidden
#line 4 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
using X.PagedList.Mvc.Common;

#line default
#line hidden
#line 5 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
using X.PagedList;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f611440331d97a794942357c9e2b3c31efe63b85", @"/Views/Order/OrdersList.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9ca0a2d6bd4e8df6987b4541b5144ce84863eb2e", @"/Views/_ViewImports.cshtml")]
    public class Views_Order_OrdersList : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<EasyAnime.Web.Models.OrderViewModel.OrdersListViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Item", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Catalog", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/admin.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(189, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 7 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
  
    ViewData["Title"] = "OrdersList";

#line default
#line hidden
            BeginContext(237, 343, true);
            WriteLiteral(@"
<h1>OrdersList</h1>
<div class=""form-group search"">
    <button onclick=""FindByLogin()"" style=""border-radius: 0px 10px 10px 0px;"" class=""btn btn-primary col-md-1 text-white float-right"">Найти</button>
    <input class=""searchLogin float-right form-control col-md-4"" style=""border-radius: 10px 0px 0px 10px;"" placeholder=""Введите логин...""");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 580, "\"", 658, 1);
#line 14 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
WriteAttributeValue("", 588, !string.IsNullOrEmpty(Model.Search) ? ViewBag.Search : string.Empty, 588, 70, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(659, 151, true);
            WriteLiteral(">\r\n    <div class=\"row\">\r\n        <select class=\"sortSelect form-control col-md-5\" onchange=\"NewParameterInUrlFromSelect(\'sortSelect\', \'SortOrder\')\">\r\n");
            EndContext();
#line 17 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
             foreach (var item in Model.Sort)
            {

#line default
#line hidden
            BeginContext(872, 16, true);
            WriteLiteral("                ");
            EndContext();
            BeginContext(888, 92, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f611440331d97a794942357c9e2b3c31efe63b856447", async() => {
                BeginContext(961, 10, false);
#line 19 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                                                                                   Write(item.Item1);

#line default
#line hidden
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            BeginWriteTagHelperAttribute();
#line 19 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                   WriteLiteral(item.Item2);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "selected", 1, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#line 19 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
AddHtmlAttributeValue("", 926, Model.SortOrder == item.Item2, 926, 32, false);

#line default
#line hidden
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(980, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 20 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
            }

#line default
#line hidden
            BeginContext(997, 41, true);
            WriteLiteral("        </select>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
#line 25 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
 if (Model.TotalCount > 0)
{

#line default
#line hidden
            BeginContext(1069, 314, true);
            WriteLiteral(@"    <table class=""table"">
        <thead>
            <tr>
                <th>Login</th>
                <th>Email</th>
                <th>Адресс</th>
                <th>Название книги</th>
                <th>Статус</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
");
            EndContext();
#line 39 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
             foreach (var item in Model.OrderDtos)
            {

#line default
#line hidden
            BeginContext(1450, 46, true);
            WriteLiteral("                <tr>\r\n                    <th>");
            EndContext();
            BeginContext(1497, 10, false);
#line 42 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                   Write(item.Login);

#line default
#line hidden
            EndContext();
            BeginContext(1507, 31, true);
            WriteLiteral("</th>\r\n                    <th>");
            EndContext();
            BeginContext(1539, 10, false);
#line 43 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                   Write(item.Email);

#line default
#line hidden
            EndContext();
            BeginContext(1549, 31, true);
            WriteLiteral("</th>\r\n                    <th>");
            EndContext();
            BeginContext(1581, 12, false);
#line 44 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                   Write(item.Address);

#line default
#line hidden
            EndContext();
            BeginContext(1593, 31, true);
            WriteLiteral("</th>\r\n                    <th>");
            EndContext();
            BeginContext(1624, 108, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f611440331d97a794942357c9e2b3c31efe63b8510983", async() => {
                BeginContext(1715, 13, false);
#line 45 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                                                                                                             Write(item.BookName);

#line default
#line hidden
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-itemName", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 45 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                                                                              WriteLiteral(item.BookNameForRoute);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["itemName"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-itemName", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["itemName"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1732, 33, true);
            WriteLiteral("</th>\r\n                    <th>\r\n");
            EndContext();
#line 47 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                          
                            if (item.Status == OrderStatus.Process)
                            {

#line default
#line hidden
            BeginContext(1893, 190, true);
            WriteLiteral("                                <div class=\"status text-center blue-round\">\r\n                                    <div class=\"descr\">В процессе</div>\r\n                                </div>\r\n");
            EndContext();
#line 53 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                            }
                            else if (item.Status == OrderStatus.Sent)
                            {

#line default
#line hidden
            BeginContext(2216, 192, true);
            WriteLiteral("                                <div class=\"status text-center yellow-round\">\r\n                                    <div class=\"descr\">Отправлено</div>\r\n                                </div>\r\n");
            EndContext();
#line 59 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                            }
                            else if (item.Status == OrderStatus.Ready)
                            {

#line default
#line hidden
            BeginContext(2542, 187, true);
            WriteLiteral("                                <div class=\"status text-center green-round\">\r\n                                    <div class=\"descr\">Готово</div>\r\n                                </div>\r\n");
            EndContext();
#line 65 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
                            }
                        

#line default
#line hidden
            BeginContext(2787, 433, true);
            WriteLiteral(@"                    </th>
                    <th>
                        <div class=""btn-group"">
                            <a class=""dropdown-toggle nav-link text-dark"" data-toggle=""dropdown"" aria-haspopup=""true"" aria-expanded=""false"">
                            </a>
                            <div class=""dropdown-menu"">
                                <input type=""submit"" value=""Поменять статус"" class=""dropdown-item""");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 3220, "\"", 3254, 3);
            WriteAttributeValue("", 3230, "ShowModal(", 3230, 10, true);
#line 73 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
WriteAttributeValue("", 3240, item.OrderId, 3240, 13, false);

#line default
#line hidden
            WriteAttributeValue("", 3253, ")", 3253, 1, true);
            EndWriteAttribute();
            BeginContext(3255, 229, true);
            WriteLiteral(" />\r\n                                <input type=\"submit\" value=\"Удалить\" class=\"dropdown-item\" onclick=\"\" />\r\n                            </div>\r\n                        </div>\r\n                    </th>\r\n                </tr>\r\n");
            EndContext();
#line 79 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
            }

#line default
#line hidden
            BeginContext(3499, 32, true);
            WriteLiteral("        </tbody>\r\n    </table>\r\n");
            EndContext();
#line 82 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
}
else
{

#line default
#line hidden
            BeginContext(3543, 59, true);
            WriteLiteral("    <h5 class=\"text-danger\">Пользователи не найдены!</h5>\r\n");
            EndContext();
#line 86 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
}

#line default
#line hidden
            BeginContext(3605, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 88 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
 if (Model.TotalCount > Model.CountOnPage)
{
    

#line default
#line hidden
            BeginContext(3659, 348, false);
#line 90 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
Write(Html.PagedListPager((IPagedList)Model, page => Url.Action("OrdersList", new { login = Model.Search, page = page, sortState = Model.SortOrder, }),
    new PagedListRenderOptionsBase
    {
        ContainerDivClasses = new[] { "navigation" },
        LiElementClasses = new[] { "page-item" },
        PageClasses = new[] { "page-link" },
    }));

#line default
#line hidden
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(4032, 182, true);
                WriteLiteral("\r\n        <script>\r\n            $(document).ready(function () {\r\n                $(\'ul.pagination > li.disabled > a\').addClass(\'page-link\');\r\n            });\r\n        </script>\r\n    ");
                EndContext();
            }
            );
#line 104 "D:\C#\EasyAnime\EasyAnime.Web\Views\Order\OrdersList.cshtml"
     
}

#line default
#line hidden
            BeginContext(4220, 347, true);
            WriteLiteral(@"
    <dialog id=""demo-modal"">
    <div class=""modal-header"">
        <h5>Изменение заказа</h5>
        <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"" onclick=""CloseModal()"">
            <span aria-hidden=""true"">&times;</span>
        </button>
    </div>
    <div class=""modal-body"">
    </div>
</dialog>

");
            EndContext();
            BeginContext(4567, 37, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f611440331d97a794942357c9e2b3c31efe63b8519307", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4604, 271, true);
            WriteLiteral(@"
<script>
    const modal = document.querySelector('dialog');
    function ShowModal(OrderId) {
        $("".modal-body"").load(""/Order/ChangeOrder/"" + OrderId);
        modal.showModal();
    }

    function CloseModal() {
        modal.close();
    }
</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<EasyAnime.Web.Models.OrderViewModel.OrdersListViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
