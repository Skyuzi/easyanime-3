﻿using EasyAnime.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyAnime.Web.Controllers
{
    public class BaseController : Controller
    {
        protected JsonResult Success() => Json(new { Success = true });
        protected JsonResult Success(string msg) => Json(new { Success = true, Data = msg });
        protected JsonResult Error() => Json(new { Success = false });
        protected JsonResult Error(string msg) => Json(new { Success = false, Data = msg });
        protected JsonResult Error(List<string> msgs) => Json(new { Success = false, Data = msgs });

        protected int GetUserIdClaim()
        {
            var userId = User.Claims.First(x => x.Type == "Id").Value;
            return int.Parse(userId);
        }

        protected List<string> GetErrorsList(ModelStateDictionary modelState)
        {
            var errorsList = new List<string>();
            foreach (var error in modelState.Where(x => x.Value.Errors.Count > 0).Select(x => x.Value.Errors).ToList())
                errorsList.Add(error[0].ErrorMessage);

            return errorsList;
        }
    }
}
