﻿using EasyAnime.BLL.Dto.Order;
using EasyAnime.BLL.Helpers.Enums;
using EasyAnime.BLL.Interfaces;
using EasyAnime.Web.Models;
using EasyAnime.Web.Models.OrderViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace EasyAnime.Web.Controllers
{
    public class OrderController : BaseController
    {
        private IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet]
        public ActionResult Make(string bookName)
        {
            var orderFilterDto = new OrderFilterDto
            {
                BookName = bookName,
                UserId = GetUserIdClaim()
            };
            var orderDto = _orderService.GetOrder(orderFilterDto);

            return View(orderDto);
        }

        [HttpPost]
        public JsonResult Make(MakeOrder dto)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception(string.Empty);

                var orderDto = new OrderDto
                {
                    UserId = GetUserIdClaim(),
                    Address = dto.Address,
                    Email = dto.Email,
                    BookName = dto.BookName
                };

                _orderService.Make(orderDto);

                return Success("Заказ успешно создан!");
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                    ModelState.AddModelError("Error", ex.Message);

                return Error(GetErrorsList(ModelState));
            }
        }

        public ActionResult MyOrders(OrderFilterDto dto)
        {
            var orderFilterDto = new OrderFilterDto
            {
                SortOrder = dto.SortOrder,
                BookName = dto.BookName,
                UserId = GetUserIdClaim()
            };

            var orders = _orderService.GetOrdersById(orderFilterDto);

            var myOrderViewModel = new MyOrderViewModel
            {
                TotalCount = orders.Count(),
                Search = dto.BookName,
                SortOrder = dto.SortOrder,
                Page = dto.Page ?? 1,
                CountOnPage = 25
            };

            myOrderViewModel.OrderDtos = orders.ToPagedList((int)myOrderViewModel.Page, myOrderViewModel.CountOnPage);
            myOrderViewModel.Sort = new Tuple<string, SortState>[]
            {
                Tuple.Create("По дате создания(новые)", SortState.AgeDesc),
                Tuple.Create("По дате создания(старые)", SortState.AgeAsc),
                Tuple.Create("По статусу", SortState.StatusAsc),
                Tuple.Create("По статусу(умень)", SortState.StatusDesc)
            };

            return View(myOrderViewModel);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult ChangeOrder(int id)
        {
            var orderDto = _orderService.GetOrderById(id);

            return View(orderDto);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public JsonResult ChangeOrder(OrderSettingsDto dto)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception(string.Empty);

                _orderService.ChangeOrder(dto);
                return Success("Статус успешно изменен!");
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                    ModelState.AddModelError("Error", ex.Message);

                return Error(GetErrorsList(ModelState));
            }
        }

        [Authorize(Roles = "Admin")]
        [Route("Admin/Orderslist")]
        [HttpGet]
        public ActionResult OrdersList(OrderFilterDto dto)
        {
            var orderFilterDto = new OrderFilterDto
            {
                SortOrder = dto.SortOrder,
                BookName = dto.BookName,
                UserId = GetUserIdClaim()
            };

            var orders = _orderService.GetOrdersById(orderFilterDto);

            var ordersListViewModel = new OrdersListViewModel
            {
                TotalCount = orders.Count(),
                Search = dto.BookName,
                SortOrder = dto.SortOrder,
                Page = dto.Page ?? 1,
                CountOnPage = 25
            };

            ordersListViewModel.OrderDtos = orders.ToPagedList((int)ordersListViewModel.Page, ordersListViewModel.CountOnPage);
            ordersListViewModel.Sort = new Tuple<string, SortState>[]
            {
                Tuple.Create("По дате создания(новые)", SortState.AgeDesc),
                Tuple.Create("По дате создания(старые)", SortState.AgeAsc),
                Tuple.Create("По статусу", SortState.StatusAsc),
                Tuple.Create("По статусу(умень)", SortState.StatusDesc)
            };

            return View(ordersListViewModel);
        }
    }
}
