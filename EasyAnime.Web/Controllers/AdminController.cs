﻿using EasyAnime.BLL.Dto.Admin;
using EasyAnime.BLL.Dto.Catalog;
using EasyAnime.BLL.Helpers.Enums;
using EasyAnime.BLL.Interfaces;
using EasyAnime.Web.Models.AdminViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace EasyAnime.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : BaseController
    {
        private IAdminService _adminService;

        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        [HttpGet]
        public async Task<ActionResult> UsersList(UsersListDto dto)
        {
            var users = _adminService.GetUsersList(dto);
            var usersViewModels = users;

            var usersListViewModels = new UsersListViewModel
            {
                TotalCount = usersViewModels.Count(),
                CountOnPage = 25,
                SortOrder = dto.SortOrder,
                Search = dto.Login,
                Page = dto.Page ?? 1
            };

            usersListViewModels.UserViewModels = await usersViewModels
                .ToPagedListAsync((int)usersListViewModels.Page, usersListViewModels.CountOnPage);


            ViewBag.FilterSelected = dto.Filter;
            ViewBag.Filters = new Tuple<string, UserListFilters>[]
            {
                Tuple.Create("Все", UserListFilters.All),
                Tuple.Create("Активные", UserListFilters.Active),
                Tuple.Create("Не активные", UserListFilters.NotActive),
                Tuple.Create("Забаненные", UserListFilters.Banned),
                Tuple.Create("Не забаненные", UserListFilters.NotBanned),
            };

            usersListViewModels.Sort = new Tuple<string, SortState>[]
            {
                Tuple.Create("По логину", SortState.NameAsc),
                Tuple.Create("По дате создания", SortState.AgeAsc),
                Tuple.Create("По дате создания(умень)", SortState.AgeDesc)
            };

            return View(usersListViewModels);
        }

        [HttpGet]
        public ActionResult UserSettings(int id)
        {
            if (id < 1)
                return NotFound();

            var user = _adminService.GetUserById(id);

            return View(user);
        }

        [HttpPost]
        public JsonResult UserSettings(UserSettingsDto dto)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception();
                _adminService.ChangeSettings(dto);

                return Success("Настройки успешно изменены!");
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                    ModelState.AddModelError("Error", ex.Message);

                return Error(GetErrorsList(ModelState));
            }
        }

        [HttpGet]
        [Route("/admin/savebook/{itemName}")]
        public ActionResult SaveBook(string itemName)
        {
            var book = _adminService.GetBook(itemName);
            ViewBag.GenreNames = _adminService.GetGenreNames();
            return View(book);
        }

        [HttpPost]
        public JsonResult SaveBook(BookDto dto)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception(string.Empty);

                dto.UserId = GetUserIdClaim();
                _adminService.AddBookOrSave(dto);

                return Success("Книга успешно сохранена");
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                    ModelState.AddModelError("Error", ex.Message);

                return Error(GetErrorsList(ModelState));
            }
        }

        public JsonResult DeleteComment(int commentId)
        {
            try
            {
                _adminService.DeleteComment(commentId);

                return Success();
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
    }
}
