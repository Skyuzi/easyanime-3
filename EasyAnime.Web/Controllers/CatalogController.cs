﻿using EasyAnime.BLL.Dto.Catalog;
using EasyAnime.BLL.Helpers.Enums;
using EasyAnime.BLL.Interfaces;
using EasyAnime.Web.Models.CatalogViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace EasyAnime.Web.Controllers
{
    public class CatalogController : BaseController
    {
        private ICatalogService _catalogService;

        public CatalogController(ICatalogService catalogService)
        {
            _catalogService = catalogService;
        }

        [Route("")]
        [Route("Catalog")]
        [Route("Catalog/Index")]
        public ActionResult Index()
        {
            ViewBag.GenreNames = _catalogService.GetGenreNames();
            return View();
        }

        [HttpGet("/catalog/category/{categoryName}")]
        public ActionResult Category(CategoryDto dto)
        {
            var books = _catalogService.GetBooksForCategory(dto);
            var genreName = _catalogService.GetGenreNameByNameForRoute(dto.CategoryName);

            if (genreName == null)
                return NotFound();

            var categoryViewModel = new CategoryViewModel
            {
                CountOnPage = 25,
                Search = dto.BookName,
                Page = dto.Page ?? 1,
                SortOrder = dto.SortOrder,
                GenreName = genreName.Name,
                GenreNameForRoute = genreName.NameForRoute
            };

            categoryViewModel.TotalCount = books.Count();
            categoryViewModel.BookViewModels = books.ToPagedList((int)categoryViewModel.Page, categoryViewModel.CountOnPage);
            categoryViewModel.Sort = new Tuple<string, SortState>[]
            {
                Tuple.Create("По названию", SortState.NameAsc),
                Tuple.Create("По году", SortState.AgeAsc),
                Tuple.Create("По году (умень)", SortState.AgeDesc)
            };

            return View(categoryViewModel);
        }

        [HttpGet("/catalog/item/{itemName}")]
        public ActionResult Item(string itemName)
        {
            var bookDto = _catalogService.GetBookForItem(itemName);
            var comments = _catalogService.GetCommentsForBook(bookDto.NameForRoute);

            var itemViewModel = new ItemViewModel
            {
                Book = bookDto,
                Comments = comments
            };

            return View(itemViewModel);
        }

        [Authorize]
        public JsonResult AddComment(CommentDto dto)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception(string.Empty);

                dto.UserId = GetUserIdClaim();
                _catalogService.AddComment(dto);

                return Success("Комментарий успешно добавлен");
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                    ModelState.AddModelError("Error", ex.Message);

                return Error(GetErrorsList(ModelState));
            }
        }
    }
}
