﻿using EasyAnime.BLL.Dto.Account;
using EasyAnime.BLL.Interfaces;
using EasyAnime.DAL.Entities.Users;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EasyAnime.Web.Controllers
{
    public class AccountController : BaseController
    {
        private IUserService _userService;
        private IHostingEnvironment _hostringEnvironment;
        public AccountController(IUserService userService, IHostingEnvironment hostingEnvironment)
        {
            _userService = userService;
            _hostringEnvironment = hostingEnvironment;
        }

        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return Redirect("/Index");
            return View(new LoginDto { ReturnUrl = returnUrl, InfoMessage = "" });
        }

        public ActionResult Register(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return Redirect("/Index");
            return View(new RegisterDto { ReturnUrl = returnUrl, InfoMessage = "" });
        }

        [HttpPost]
        public ActionResult Login(LoginDto dto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(dto);

                var user = _userService.Login(dto);
                var claimsIdentity = _userService.GetClaimsForAuthentication(user);
                HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));
                if (!string.IsNullOrEmpty(dto.ReturnUrl))
                    return Redirect(dto.ReturnUrl);

                return Redirect("/");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View(dto);
            }
        }

        [HttpPost]
        public ActionResult Register(RegisterDto dto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(dto);

                var user = _userService.Register(dto);
                var claimsIdentity = _userService.GetClaimsForAuthentication(user);
                HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));
                if (!string.IsNullOrEmpty(dto.ReturnUrl))
                    return Redirect(dto.ReturnUrl);

                return Redirect("/");
            }
            catch (Exception ex)
            {
                return View(new RegisterDto { InfoMessage = ex.Message });
            }
        }

        public async Task<ActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            return Redirect("/");
        }
        
        public ActionResult Settings()
        {
            var avatar = _userService.GetAvatarName(GetUserIdClaim());
            ViewBag.Avatar = avatar ?? null;

            return View();
        }

        public ActionResult ChangeAvatar(IFormFile file)
        {
            try
            {
                var changeAvatarDto = new ChangeAvatarDto
                {
                    File = file,
                    WebPath = _hostringEnvironment.WebRootPath,
                    UserId = GetUserIdClaim()
                };

                _userService.ChangeAvatar(changeAvatarDto);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
            }

            return RedirectToAction("Settings", "Account");
        }
    }
}
