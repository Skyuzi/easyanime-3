﻿using EasyAnime.BLL.Dto.Catalog;
using EasyAnime.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EasyAnime.Web.Controllers
{
    public class ApiController : BaseController
    {
        private ICatalogService _catalogService;

        public ApiController(ICatalogService catalogService)
        {
            _catalogService = catalogService;
        }

        public string GetBooks(CategoryDto dto)
        {
            try
            {
                var books = _catalogService.GetBooksForCategory(dto).ToList();
                var jsonBooks = JsonConvert.SerializeObject(books, Formatting.Indented);

                return jsonBooks;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
