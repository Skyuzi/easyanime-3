﻿using EasyAnime.DAL.Entities.Books;
using EasyAnime.DAL.Entities.Orders;
using EasyAnime.DAL.Entities.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Avatar> Avatars { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Genres> Genres{ get; set; }
        public DbSet<GenreNames> GenreNames { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<BannedUser> BannedUsers { get; set; }
        public DbSet<Order> Orders { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }
    }
}
