﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EasyAnime.DAL.Entities.Users
{
    [Table("BannedUser")]
    public class BannedUser
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public bool IsActual { get; set; }
        public User User { get; set; }
        public string Description { get; set; }
        public DateTime? BanTime { get; set; }
        public DateTime? WillActiveTime { get; set; }
    }
}
