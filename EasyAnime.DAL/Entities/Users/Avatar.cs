﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasyAnime.DAL.Entities.Users
{
    [Table("Avatar")]
    public class Avatar
    {
        public int Id { get; set; }
        public string AvatarName { get; set; }
        public int UserId { get; set; }
    }
}
