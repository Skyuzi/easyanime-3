﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EasyAnime.DAL.Entities.Users
{
    [Table("Users")]
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public Avatar Avatar { get; set; }
        public bool IsBan { get; set; }
        public bool IsDeleted { get; set; }
        public string Email { get; set; }
        public string IpAddress { get; set; }
        public DateTime AccountCreated { get; set; }
        public DateTime LastEnter { get; set; }
    }
}
