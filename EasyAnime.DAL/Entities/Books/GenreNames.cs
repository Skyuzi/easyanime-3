﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EasyAnime.DAL.Entities.Books
{
    [Table("GenreNames")]
    public class GenreNames
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameForRoute { get; set; }
    }
}
