﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EasyAnime.DAL.Entities.Books
{
    [Table("Genres")]
    public class Genres
    {
        public int Id { get; set; }
        public int GenreNamesId { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }
    }
}
