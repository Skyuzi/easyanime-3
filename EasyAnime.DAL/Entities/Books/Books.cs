﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EasyAnime.DAL.Entities.Books
{
    [Table("Book")]
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameForRoute { get; set; }
        public string Author { get; set; }
        public string ImageUrl { get; set; }
        public int Year { get; set; }
        public int UserId { get; set; }
        public int Price { get; set; }
        public DateTime AddedTime { get; set; }
        public DateTime ChangedTime { get; set; }
        public string Description { get; set; }
        public List<Genres> Genres { get; set; }

        public bool IsDiscount { get; set; }
        public int DiscountPercent { get; set; }

        [NotMapped]
        public int DiscountPrice => IsDiscount ? GetDiscountPrice() : 0;

        private int GetDiscountPrice() => Price - (Price * DiscountPercent / 100);
    }
}
