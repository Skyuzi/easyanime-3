﻿using EasyAnime.DAL.Entities.Books;
using EasyAnime.DAL.Entities.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EasyAnime.DAL.Entities.Orders
{
    [Table("Order")]
    public class Order
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public DateTime AddedTime { get; set; }
    }
}
