﻿using EasyAnime.DAL.Entities.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using EasyAnime.DAL.Interfaces;
using System.Linq;

namespace EasyAnime.DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private ApplicationContext _dbContext;
        private DbSet<User> _dbSet;

        public UserRepository(ApplicationContext context)
        {
            _dbContext = context;
            _dbSet = _dbContext.Users;
        }

        public User Add(User item)
        {
            _dbSet.Add(item);
            return item;
        }


        public IEnumerable<User> GetActual()
        {
            return _dbSet.Where(x => !x.IsDeleted).ToList();
        }

        public IEnumerable<User> GetAll()
        {
            return _dbSet.ToList();
        }

        public IEnumerable<User> GetBanned()
        {
            return _dbSet.Where(x => x.IsBan).ToList();
        }

        public User GetById(int id)
        {
            return _dbSet.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<User> GetNotActual()
        {
            return _dbSet.Where(x => x.IsDeleted).ToList();
        }

        public IEnumerable<User> GetNotBanned()
        {
            return _dbSet.Where(x => !x.IsBan).ToList();
        }

        public IEnumerable<User> GetSkip(int skip)
        {
            return _dbSet.Skip(skip).ToList();
        }

        public IEnumerable<User> GetTop(int count)
        {
            return _dbSet.Take(count).ToList();
        }

        public IEnumerable<User> GetTopWithSkip(int count, int skip)
        {
            return _dbSet.Skip(skip).Take(count).ToList();
        }

        public User GetByLoginAndPassword(string login, string password)
        {
            return _dbSet.FirstOrDefault(x => x.Login == login && x.Password == password);
        }

        public void Remove(User item)
        {
            _dbSet.Remove(item);
        }

        public void Update(User item)
        {
            _dbSet.Update(item);
        }

        public bool IsExist(int id)
        {
            return _dbSet.Any(x => x.Id == id);
        }

        public bool IsExist(string login)
        {
            return _dbSet.Any(x => x.Login == login);
        }

        public User GetByLogin(string login)
        {
            return _dbSet.FirstOrDefault(x => x.Login == login);
        }

        public void AddRange(List<User> items)
        {
            _dbSet.AddRange(items);
        }
    }
}
