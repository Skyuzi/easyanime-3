﻿using EasyAnime.DAL.Entities.Books;
using EasyAnime.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.DAL.Repositories
{
    public class GenreRepository : IGenreRepository
    {
        private ApplicationContext _dbContext;
        private DbSet<Genres> _dbSet;

        public GenreRepository(ApplicationContext context)
        {
            _dbContext = context;
            _dbSet = context.Genres;
        }

        public Genres Add(Genres item)
        {
            _dbSet.Add(item);
            return item;
        }

        public void AddRange(List<Genres> items)
        {
            _dbSet.AddRange(items);
        }

        public IEnumerable<Genres> GetAll()
        {
            return _dbSet.ToList();
        }

        public IEnumerable<Book> GetBooksByGenreNamesId(int genreNamesId)
        {
            return _dbSet.Include(x => x.Book)
                    .Where(x => x.GenreNamesId == genreNamesId)
                    .Select(x => x.Book);
        }

        public IEnumerable<Book> GetBooksByGenreNamesIdAndByBookName(int genreNamesId, string bookName)
        {
            return _dbSet.Include(x => x.Book)
                    .Where(x => x.GenreNamesId == genreNamesId && x.Book.Name.Contains(bookName))
                    .Select(x => x.Book);
        }

        public Genres GetById(int id)
        {
            return _dbSet.FirstOrDefault(x => x.Id == id);
        }

        public void Remove(Genres item)
        {
            _dbSet.Remove(item);
        }

        public void Update(Genres item)
        {
            _dbSet.Update(item);
        }
    }
}
