﻿using EasyAnime.DAL.Entities.Users;
using EasyAnime.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.DAL.Repositories
{
    public class BannedUserRepository : IBannedUserRepository
    {
        private ApplicationContext _dbContext;
        private DbSet<BannedUser> _dbSet;

        public BannedUserRepository(ApplicationContext context)
        {
            _dbContext = context;
            _dbSet = _dbContext.BannedUsers;
        }

        public BannedUser Add(BannedUser item)
        {
            _dbSet.Add(item);
            return item;
        }

        public void AddRange(List<BannedUser> items)
        {
            _dbSet.AddRange(items);
        }

        public IEnumerable<BannedUser> GetAll()
        {
            return _dbSet.ToList();
        }

        public BannedUser GetById(int id)
        {
            return _dbSet.FirstOrDefault(x => x.Id == id);
        }

        public BannedUser GetByUserId(int userId)
        {
            return _dbSet.FirstOrDefault(x => x.UserId == userId);
        }

        public BannedUser GetLastByUserId(int userId)
        {
            return _dbSet.LastOrDefault(x => x.UserId == userId);
        }

        public void Remove(BannedUser item)
        {
            _dbSet.Remove(item);
        }

        public void Update(BannedUser item)
        {
            _dbSet.Update(item);
        }
    }
}
