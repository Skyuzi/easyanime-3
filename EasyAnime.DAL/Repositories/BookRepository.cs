﻿using EasyAnime.DAL.Entities.Books;
using EasyAnime.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.DAL.Repositories
{
    public class BookRepository : IBookRepository
    {
        private ApplicationContext _dbContext;
        private DbSet<Book> _dbSet;

        public BookRepository(ApplicationContext context)
        {
            _dbContext = context;
            _dbSet = context.Books;
        }

        public Book Add(Book item)
        {
            _dbSet.Add(item);
            return item;
        }

        public void AddRange(List<Book> items)
        {
            _dbSet.AddRange(items);
        }

        public IEnumerable<Book> GetAll()
        {
            return _dbSet.Include(x => x.Genres).ToList();
        }

        public Book GetById(int id)
        {
            return _dbSet.FirstOrDefault(x => x.Id == id);
        }

        public Book GetByNameForRoute(string nameForRoute)
        {
            return _dbSet.Include(x => x.Genres).FirstOrDefault(x => x.NameForRoute == nameForRoute);
        }

        public void Remove(Book item)
        {
            _dbSet.Remove(item);
        }

        public void Update(Book item)
        {
            _dbSet.Update(item);
        }
    }
}
