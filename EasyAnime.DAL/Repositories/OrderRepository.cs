﻿using EasyAnime.DAL.Entities.Orders;
using EasyAnime.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.DAL.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private ApplicationContext _dbContext;
        private DbSet<Order> _dbSet;

        public OrderRepository(ApplicationContext context)
        {
            _dbContext = context;
            _dbSet = context.Orders;
        }

        public Order Add(Order item)
        {
            _dbSet.Add(item);
            return item;
        }

        public void AddRange(List<Order> items)
        {
            _dbSet.AddRange(items);
        }

        public IEnumerable<Order> GetAll()
        {
            return _dbSet.ToList();
        }

        public Order GetById(int id)
        {
            return _dbSet.Include(x => x.Book).Include(x => x.User).FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Order> GetByUserId(int userId)
        {
            return _dbSet.Include(x => x.Book).Include(x => x.User).Where(x => x.UserId == userId).ToList();
        }

        public void Remove(Order item)
        {
             _dbSet.Include(x => x.Book).Include(x => x.User).ToList();
        }

        public void Update(Order item)
        {
            _dbSet.Update(item);
        }
    }
}
