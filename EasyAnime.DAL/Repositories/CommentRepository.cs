﻿using EasyAnime.DAL.Entities.Books;
using EasyAnime.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.DAL.Repositories
{
    class CommentRepository : ICommentRepository
    {
        private ApplicationContext _dbContext;
        private DbSet<Comment> _dbSet;

        public CommentRepository(ApplicationContext context)
        {
            _dbContext = context;
            _dbSet = context.Comments;
        }

        public Comment Add(Comment item)
        {
            _dbSet.Add(item);
            return item;
        }

        public void AddRange(List<Comment> items)
        {
            _dbSet.AddRange(items);
        }

        public IEnumerable<Comment> GetAll()
        {
            return _dbSet.ToList();
        }

        public IEnumerable<Comment> GetByBookId(int bookId)
        {
            return _dbSet.Include(x => x.User).Include(x => x.User.Avatar).Where(x => x.BookId == bookId).ToList();
        }

        public Comment GetById(int id)
        {
            return _dbSet.FirstOrDefault(x => x.Id == id);
        }

        public void Remove(Comment item)
        {
            _dbSet.Remove(item);
        }

        public void Update(Comment item)
        {
            _dbSet.Update(item);
        }
    }
}
