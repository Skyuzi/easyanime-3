﻿using EasyAnime.DAL.Entities.Users;
using EasyAnime.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.DAL.Repositories
{
    public class AvatarRepository : IAvatarRepository
    {
        private DbSet<Avatar> _dbSet;

        public AvatarRepository(ApplicationContext context)
        {
            _dbSet = context.Avatars;
        }

        public Avatar Add(Avatar item)
        {
            _dbSet.Add(item);
            return item;
        }

        public void AddRange(List<Avatar> items)
        {
            _dbSet.AddRange(items);
        }

        public IEnumerable<Avatar> GetAll()
        {
            return _dbSet.ToList();
        }

        public Avatar GetById(int id)
        {
            return _dbSet.FirstOrDefault(x => x.Id == id);
        }

        public Avatar GetByUserId(int userId)
        {
            return _dbSet.FirstOrDefault(x => x.UserId == userId);
        }

        public void Remove(Avatar item)
        {
            _dbSet.Remove(item);
        }

        public void Update(Avatar item)
        {
            _dbSet.Update(item);
        }
    }
}
