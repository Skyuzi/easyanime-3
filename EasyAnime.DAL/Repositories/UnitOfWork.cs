﻿using EasyAnime.DAL.Entities.Users;
using EasyAnime.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationContext _dbContext;

        public IUserRepository Users { get; private set; }
        public IRepository<Role> Roles { get; private set; }
        public IBannedUserRepository BannedUsers { get; private set; }
        public ICommentRepository Comments { get; private set; }
        public IGenreNamesRepository GenreNames { get; private set; }
        public IGenreRepository Genres { get; private set; }
        public IBookRepository Books { get; private set; }
        public IOrderRepository Orders { get; private set; }
        public IAvatarRepository Avatars { get; private set; }

        public UnitOfWork(ApplicationContext context)
        {
            _dbContext = context;
            Users = new UserRepository(_dbContext);
            Roles = new Repository<Role>(_dbContext);
            BannedUsers = new BannedUserRepository(_dbContext);
            Comments = new CommentRepository(_dbContext);
            GenreNames = new GenreNamesRepository(_dbContext);
            Genres = new GenreRepository(_dbContext);
            Books = new BookRepository(_dbContext);
            Orders = new OrderRepository(_dbContext);
            Avatars = new AvatarRepository(_dbContext);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}
