﻿using EasyAnime.DAL.Entities.Books;
using EasyAnime.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.DAL.Repositories
{
    public class GenreNamesRepository : IGenreNamesRepository
    {
        private DbSet<GenreNames> _dbSet;

        public GenreNamesRepository(ApplicationContext context)
        {
            _dbSet = context.GenreNames;
        }

        public GenreNames Add(GenreNames item)
        {
            _dbSet.Add(item);
            return item;
        }

        public void AddRange(List<GenreNames> items)
        {
            _dbSet.AddRange(items);
        }

        public IEnumerable<GenreNames> GetAll()
        {
            return _dbSet.ToList();
        }

        public GenreNames GetById(int id)
        {
            return _dbSet.FirstOrDefault(x => x.Id == id);
        }

        public GenreNames GetByNameForRoute(string nameForRoute)
        {
            return _dbSet.FirstOrDefault(x => x.NameForRoute == nameForRoute);
        }

        public void Remove(GenreNames item)
        {
            _dbSet.Remove(item);
        }

        public void Update(GenreNames item)
        {
            _dbSet.Update(item);
        }
    }
}
