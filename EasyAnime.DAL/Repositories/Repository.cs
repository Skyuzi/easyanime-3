﻿using EasyAnime.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private ApplicationContext _dbContext;
        private DbSet<T> _dbSet;

        public Repository(ApplicationContext context)
        {
            _dbContext = context;
            _dbSet = _dbContext.Set<T>();
        }

        public T Add(T item)
        {
            _dbSet.Add(item);
            return item;
        }

        public void AddRange(List<T> items)
        {
            _dbSet.AddRange(items);
        }

        public IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public T GetById(int id)
        {
            return _dbSet.Find(id);
        }

        public void Remove(T item)
        {
            _dbSet.Remove(item);
        }

        public void Update(T item)
        {
            _dbSet.Update(item);
        }
    }
}
