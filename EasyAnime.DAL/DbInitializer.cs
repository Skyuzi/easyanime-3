﻿using EasyAnime.DAL.Entities.Books;
using EasyAnime.DAL.Entities.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyAnime.DAL
{
    public static class DbInitializer
    {
        public static void Initialize(ApplicationContext context)
        {
            context.Database.EnsureCreated();
            if (context.Roles.Any())
                return;

            var roles = new Role[]
            {
                new Role {Name = "User"},
                new Role {Name = "Moderator"},
                new Role {Name = "Admin"},
            };

            foreach (var role in roles)
                context.Roles.Add(role);

            context.SaveChanges();

            var genreNames = new GenreNames[]
            {
                new GenreNames{Name = "Фантастика", NameForRoute = "fantastika"},
                new GenreNames{Name = "Приключение", NameForRoute = "priklyucheniye"},
                new GenreNames{Name = "Мистика", NameForRoute = "mistika"},
                new GenreNames{Name = "Фэнтези", NameForRoute = "fentezi"},
                new GenreNames{Name = "Ужасы", NameForRoute = "uzhasy"},
            };

            foreach (var genreName in genreNames)
                context.GenreNames.Add(genreName);

            context.SaveChanges();

            var admin = new User
            {
                Login = "admin",
                Password = "admin",
                RoleId = 3,
                IsBan = false,
                IsDeleted = false,
                AccountCreated = DateTime.Now,
                LastEnter = DateTime.Now
            };

            context.Users.Add(admin);
            context.SaveChanges();

            var book = new Book
            {
                AddedTime = DateTime.Now,
                Name = "Death Note. Black Edition. Книга 1",
                NameForRoute = "death-note-black-edition-1",
                Author = "Ооба Цугуми",
                ImageUrl = "https://cdn1.ozone.ru/s3/multimedia-e/c1200/6006372338.jpg",
                Year = 2017,
                Price = 650,
                Description = "У студента колледжа Лайта Ягами есть блестящие перспективы на будущее и ни малейшей идеи, чем заполнить настоящее. Парень сходит с ума от скуки. Но все меняется, когда он находит записную книжку синигами – бога смерти. Любой человек, чье имя окажется на ее странице, умрет. Лайт решает использовать Тетрадь смерти, чтобы избавить мир от зла. Куда приведут эти благие намерения?"
            };

            context.Books.Add(book);
            context.SaveChanges();

            var genre = new Genres
            {
                GenreNamesId = 3,
                BookId = book.Id
            };

            context.Genres.Add(genre);
            context.SaveChanges();
        }
    }
}
