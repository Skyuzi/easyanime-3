﻿using EasyAnime.DAL.Entities.Books;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL.Interfaces
{
    public interface IGenreRepository : IRepository<Genres> 
    {
        IEnumerable<Book> GetBooksByGenreNamesId(int genreNamesId);
        IEnumerable<Book> GetBooksByGenreNamesIdAndByBookName(int genreNamesId, string bookName);
    }
}
