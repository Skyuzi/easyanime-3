﻿using EasyAnime.DAL.Entities.Orders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        IEnumerable<Order> GetByUserId(int userId);
    }
}
