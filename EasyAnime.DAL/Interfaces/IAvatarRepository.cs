﻿using EasyAnime.DAL.Entities.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL.Interfaces
{
    public interface IAvatarRepository : IRepository<Avatar>
    {
        Avatar GetByUserId(int userId);
    }
}
