﻿using EasyAnime.DAL.Entities.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL.Interfaces
{
    public interface IBannedUserRepository : IRepository<BannedUser>
    {
        BannedUser GetByUserId(int userId);
        BannedUser GetLastByUserId(int userId);
    }
}
