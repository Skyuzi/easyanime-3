﻿using EasyAnime.DAL.Entities.Books;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL.Interfaces
{
    public interface IBookRepository : IRepository<Book>
    {
        Book GetByNameForRoute(string nameForRoute);
    }
}
