﻿using EasyAnime.DAL.Entities.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        IEnumerable<User> GetTop(int count);
        IEnumerable<User> GetSkip(int skip);
        IEnumerable<User> GetTopWithSkip(int count, int skip);
        IEnumerable<User> GetActual();
        IEnumerable<User> GetBanned();
        IEnumerable<User> GetNotBanned();
        IEnumerable<User> GetNotActual();
        User GetByLoginAndPassword(string login, string password);
        User GetByLogin(string login);
        bool IsExist(int id);
        bool IsExist(string login);
    }
}
