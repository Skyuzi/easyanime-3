﻿using EasyAnime.DAL.Entities.Books;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL.Interfaces
{
    public interface ICommentRepository : IRepository<Comment>
    {
        IEnumerable<Comment> GetByBookId(int bookId);
    }
}
