﻿using EasyAnime.DAL.Entities.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        IRepository<Role> Roles { get; }
        ICommentRepository Comments { get; }
        IBannedUserRepository BannedUsers { get; }
        IGenreNamesRepository GenreNames { get; }
        IGenreRepository Genres { get; }
        IBookRepository Books { get; }
        IOrderRepository Orders { get; }
        IAvatarRepository Avatars { get; }
        void Save();
    }
}
