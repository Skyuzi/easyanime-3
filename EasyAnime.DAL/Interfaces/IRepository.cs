﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAnime.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        T Add(T item);
        void AddRange(List<T> items);
        void Update(T item);
        void Remove(T item);
    }
}
